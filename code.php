<?php

function getLetterGrade($grade) {
    if($grade <= 100 && $grade >= 98) {
        echo "$grade is equivalent to A+";
    }
    else if($grade <= 97 && $grade >= 95) {
        echo "$grade is equivalent to A";
    }
    else if($grade <= 94 && $grade >= 92) {
        echo "$grade is equivalent to A-";
    }
    else if($grade <= 91 && $grade >= 89) {
        echo "$grade is equivalent to B+";
    }
    else if($grade <= 88 && $grade >= 86) {
        echo "$grade is equivalent to B";
    }
    else if($grade <= 85 && $grade >= 83) {
        echo "$grade is equivalent to B-";
    }
    else if($grade <= 82 && $grade >= 80) {
        echo "$grade is equivalent to C+";
    }
    else if($grade <= 79 && $grade >= 77) {
        echo "$grade is equivalent to C";
    }
    else if($grade <= 76 && $grade >= 75) {
        echo "$grade is equivalent to C-";
    }
    else {
        echo "$grade is equivalent to D";
    }
}