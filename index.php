<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>S01 Activity: PHP Basics and Selection Control Structures</title>
    </head>
    <body>
        <h1>Letter-Based Grading</h1>
        <p><?php echo getLetterGrade(87) ?> </p>
        <p><?php echo getLetterGrade(94) ?> </p>
        <p><?php echo getLetterGrade(74) ?> </p>
        <!-- <p><?php echo getLetterGrade(90) ?> </p>
        <p><?php echo getLetterGrade(87) ?> </p>
        <p><?php echo getLetterGrade(84) ?> </p>
        <p><?php echo getLetterGrade(81) ?> </p>
        <p><?php echo getLetterGrade(78) ?> </p>
        <p><?php echo getLetterGrade(76) ?> </p>
        <p><?php echo getLetterGrade(73) ?> </p> -->
    </body>
</html>